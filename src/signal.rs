use egui::plot::*;
use bitvec::prelude::*;

const HIGH_VOLTAGE: i8 = 5;
const ZERO_VOLTAGE: i8 = 0;
const LOW_VOLTAGE: i8 = -5;
const INTERPOLATION: f64 = 10.0;

#[derive(Debug)]
pub struct SignalValues {
    pub freq: f64,
    pub values: Vec<u8>,
    pub encoded: Vec<(i8, i8)>,
    pub points: Vec<PlotPoint>,
    pub cur_x: f64,
}

impl SignalValues {
    pub fn new(freq: f64) -> Self {
        Self {
            freq,
            values: Vec::new(),
            encoded: Vec::new(),
            points: Vec::new(),
            cur_x: 0.0,
        }
    }

    pub fn add(&mut self, data: u8) {
        self.values.push(data);
        for bit in data.view_bits::<Msb0>().iter() {
            // If Bit is set (bit is 1: high->zero voltage)
            if *bit {
                self.points.push(PlotPoint::new(self.cur_x, HIGH_VOLTAGE));
                self.points.push(PlotPoint::new((self.cur_x+(INTERPOLATION/self.freq)/2.0), HIGH_VOLTAGE));
                self.points.push(PlotPoint::new((self.cur_x+(INTERPOLATION/self.freq)/2.0), ZERO_VOLTAGE));
                self.points.push(PlotPoint::new((self.cur_x+(INTERPOLATION/self.freq)), ZERO_VOLTAGE));

                self.encoded.push((HIGH_VOLTAGE, ZERO_VOLTAGE));
            // If Bit is not set (bit is 0: low->zero voltage)
            } else {
                self.points.push(PlotPoint::new(self.cur_x, LOW_VOLTAGE));
                self.points.push(PlotPoint::new((self.cur_x+(INTERPOLATION/self.freq)/2.0), LOW_VOLTAGE));
                self.points.push(PlotPoint::new((self.cur_x+(INTERPOLATION/self.freq)/2.0), ZERO_VOLTAGE));
                self.points.push(PlotPoint::new((self.cur_x+(INTERPOLATION/self.freq)), ZERO_VOLTAGE));

                self.encoded.push((LOW_VOLTAGE, ZERO_VOLTAGE));
            }
            self.cur_x += INTERPOLATION/self.freq
        }
    }

    pub fn decode(&self) -> Vec<u8> {
        let mut decoded: Vec<u8> = Vec::new();
        let mut bits = bitarr![u8, Msb0; 0; 8];

        for (n, (v, _)) in self.encoded.iter().enumerate() {
            match *v {
                HIGH_VOLTAGE => {
                    // decoded.push(1);
                    bits.set(n % 8, true);
                },
                LOW_VOLTAGE => {
                    // decoded.push(0);
                    bits.set(n % 8, false);
                },
                _ => {
                    continue;
                }
            }
            if n % 8 == 0 && n != 0 {
                let byte = bits.load::<u8>();
                decoded.push(byte);
            }
        }

        decoded
    }

    pub fn plot_values(&self) -> PlotPoints {
        PlotPoints::Owned(self.points.clone())
    }
}
