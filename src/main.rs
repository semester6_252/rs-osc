mod signal;

use crate::signal::SignalValues;

use eframe::egui;
use std::fs::File;
use std::io::Read;
use std::sync::*;
use std::thread;
use std::time;
use std::path;
// use tracing::{error, info, warn};
use tracing::info;

pub struct MonitorApp {
    signal: Arc<Mutex<SignalValues>>,
    include_y: f64,
}

impl MonitorApp {
    pub fn new(freq: f64, include_y: f64) -> Self {
        Self {
            signal: Arc::new(Mutex::new(SignalValues::new(freq))),
            include_y,
        }
    }
}

impl eframe::App for MonitorApp {
    /// Called by the frame work to save state before shutdown.
    /// Note that you must enable the `persistence` feature for this to work.
    #[cfg(feature = "persistence")]
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            let plot = egui::plot::Plot::new("signal")
                .data_aspect(5.0)
                .include_y(self.include_y);

            plot.show(ui, |plot_ui| {
                plot_ui.line(egui::plot::Line::new(
                    self.signal.lock().unwrap().plot_values()
                ));
            });
        });
        // make it always repaint. TODO: can we slow down here?
        ctx.request_repaint();
    }

}

use clap::Parser;

/// RZ coder and decoder.
#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    /// Path to a file wanted to be encoded.
    #[clap(short, long)]
    file_path: path::PathBuf,
    /// Signal frequency.
    #[clap(long)]
    freq: f64,
}

fn main() {
    let args = Args::parse();
    let file = File::open(args.file_path).expect("was not able to open a file with given 'file_path'");

    // let subscriber = tracing_subscriber::FmtSubscriber::builder()
    //     .with_max_level(tracing::Level::TRACE)
    //     .finish();
    // tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    let app = MonitorApp::new(args.freq, 5.0);

    let native_options = eframe::NativeOptions::default();

    let monitor_ref = app.signal.clone();

    let data = file.bytes();

    // TODO: make plotting nicer
    thread::spawn(move || {
        for byte in data {
            monitor_ref.lock().unwrap().add(byte.expect("was not able to read a byte"));
            let hun_millis = time::Duration::from_millis(100);
            thread::sleep(hun_millis);
            let result = monitor_ref.lock().unwrap().decode();
            println!("{:?}, {:?}", result, monitor_ref.lock().unwrap().encoded);
        }
    });


    info!("Main thread started");
    eframe::run_native("Monitor app", native_options, Box::new(|_| Box::new(app)));
}
